import numpy 
import cv2
import os

def play_videoFile(filePath,mirror=False):

    os.environ['SDL_VIDEO_CENTERED'] = '1'
    window_name = "Escape"
    cap = cv2.VideoCapture(filePath)
    cv2.namedWindow(window_name,cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN,
                          cv2.WINDOW_FULLSCREEN)
    while (cap.isOpened()):
        ret_val, frame = cap.read()
        if ret_val:
            cv2.imshow(window_name, frame) 
        else:    
            cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
        if cv2.waitKey(10) == 98:
            break  # esc to quit
    cap.release()
    cv2.destroyAllWindows()

def play_videoFile_Spacebar(filePath,mirror=False):

    os.environ['SDL_VIDEO_CENTERED'] = '1'
    window_name = "Escape"
    cap = cv2.VideoCapture(filePath)
    cv2.namedWindow(window_name,cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN,
                          cv2.WINDOW_FULLSCREEN)
    while (cap.isOpened()):
        ret_val, frame = cap.read()
        if ret_val:
            cv2.imshow(window_name, frame) 
        else:    
            cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
        if cv2.waitKey(10) == 51:
            break  # esc to quit
    cap.release()
    cv2.destroyAllWindows()


def play_videoNoLoop(filePath,mirror=False):

    os.environ['SDL_VIDEO_CENTERED'] = '1'
    window_name = "Escape"
    cap = cv2.VideoCapture(filePath)
    cv2.namedWindow(window_name,cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN,
                          cv2.WINDOW_FULLSCREEN)
    fnum = 0
    while True:
        ret_val, frame = cap.read()
        fnum += 1
        if ret_val:
            cv2.imshow(window_name, frame)
            print(fnum)
            if cv2.waitKey(10) == 98:
                break
        else:
            break
    cap.release()
    cv2.destroyAllWindows()

def displayImage(filePath):     

    os.environ['SDL_VIDEO_CENTERED'] = '1'
    window_name = "Escape"
    image = cv2.imread(filePath)
    cv2.namedWindow(window_name,cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN,
                          cv2.WINDOW_FULLSCREEN)
    cv2.imshow(window_name,image)
    cv2.waitKey(3000)
    cv2.destroyAllWindows()
def main():
    play_videoNoLoop('Opening_Door.mp4', mirror=False)

if __name__ == '__main__':
    main()
