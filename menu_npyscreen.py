import npyscreen
import time
from movie_opencv import play_videoFile, play_videoNoLoop, displayImage 


class mainMenuForm(npyscreen.FormBaseNew):

    def create(self):
       self.door_button = self.add(npyscreen.ButtonPress, name='Door Operations', when_pressed_function=self.doorPress)
       self.nextrely += 1
       self.surveillance_button = self.add(npyscreen.ButtonPress, name='Surveillance System Operations', when_pressed_function=self.surveillancePress)
       self.nextrely += 1
       self.hvac_button = self.add(npyscreen.ButtonPress, name='HVAC Operations', when_pressed_function=self.hvacPress)
       self.nextrely += 1
       self.exit_button = self.add(npyscreen.ButtonPress, name='Exit', when_pressed_function=self.exit)
    def doorPress(self):
        self.parentApp.switchForm('DOORFORM')
    def surveillancePress(self):
        self.parentApp.switchForm('SURVEILLANCEFORM')
    def hvacPress(self):
        self.parentApp.switchForm('HVACFORM')
    def exit(self):
        self.parentApp.switchForm(None)
class doorMenuForm(npyscreen.FormBaseNew):
     
    def create(self):

        self.Password_button = self.add(npyscreen.ButtonPress, name='Input Password', when_pressed_function=self.passwordInput)
        self.nextrely += 1
        self.Details_button = self.add(npyscreen.ButtonPress, name='Door Description', when_pressed_function=self.doorDetails)
        self.nextrely += 2
        self.back_button = self.add(npyscreen.ButtonPress, name='<-- Back', when_pressed_function=self.back)
                
    def passwordInput(self):
        self.parentApp.switchForm('PASSWORDFORM')

    def doorDetails(self):
        self.parentApp.switchForm('DOORDETAILSFORM')
    def back(self):
        self.parentApp.switchForm('MAIN')
class passwordInputForm(npyscreen.FormBaseNew):

    def create(self):
        self.passwordfield = self.add(npyscreen.TitleText, name='Password: ')
        self.nextrely += 1
        self.submit_button = self.add(npyscreen.ButtonPress, name='Submit Password -->', when_pressed_function=self.submit)
        self.nextrely += 2 
        self.back_button = self.add(npyscreen.ButtonPress, name='<-- Back', when_pressed_function=self.back)
    def submit(self):
        if self.passwordfield.value == "ABAA":
            message_to_display = "Password Accepted. Opening Door!"
            npyscreen.notify(message_to_display, title='Password Manager')
            time.sleep(2) # needed to have it show up for a visible amount of time
#            self.parentApp.resetHistory()
            play_videoNoLoop("Opening_Door.mp4", mirror=False)
            displayImage('you_escaped.jpg')
            self.parentApp.switchForm(None)
        else: 
            message_to_display = "Password Incorrect. Try Again..."
            npyscreen.notify(message_to_display, title='Password Manager')
            time.sleep(2) # needed to have it show up for a visible amount of time
        self.passwordfield.value = ""
        self.display()
    def back(self):
        self.parentApp.switchFormPrevious()

class doorDetailsForm(npyscreen.FormBaseNew):
    def create(self):
        doortext = "This door leads to the outside and is locked with a password."
        self.doordetails = self.add(npyscreen.TitleText, name='Door Information', editable=False)
        self.nextrely += 2 
        self.back_button = self.add(npyscreen.ButtonPress, name='<-- Back', when_pressed_function=self.back)
    def back(self):
        self.parentApp.switchFormPrevious()

class surveillanceMenuForm(npyscreen.FormBaseNew):
    def create(self):
        self.surveillance_button = self.add(npyscreen.ButtonPress, name="Surveillance System Details", when_pressed_function=self.details)
        self.realign_button = self.add(npyscreen.ButtonPress, name="View Surveillance Footage", when_pressed_function=self.footage)
        self.nextrely += 2 
        self.back_button = self.add(npyscreen.ButtonPress, name='<-- Back', when_pressed_function=self.back)
    def back(self):
        self.parentApp.switchFormPrevious()
    def footage(self):
        self.parentApp.switchForm('FOOTAGEFORM')
    def details(self):
        self.parentApp.switchForm('SURVEILLANCEDETAILSFORM')

class surveillanceDetailsForm(npyscreen.FormBaseNew):
    def create(self):
        surveillancetext = "Monitor surveillance camera footage throughout the facility."
        self.surveillancedetails = self.add(npyscreen.TitleText, name='Description:', value=surveillancetext, editable=False)
        self.nextrely += 2 
        self.back_button = self.add(npyscreen.ButtonPress, name='<-- Back', when_pressed_function=self.back)
    def back(self):
        self.parentApp.switchFormPrevious()

class footageForm(npyscreen.FormBaseNew):
    def create(self):
        self.kitchen_button = self.add(npyscreen.ButtonPress, name="Staff Kitchen - Area 7C", when_pressed_function=self.kitchen_movie)
        self.office_button = self.add(npyscreen.ButtonPress, name="Office - Employee 264A", when_pressed_function=self.office_movie)
        self.hallway_button = self.add(npyscreen.ButtonPress, name="Hallway - Area 4B", when_pressed_function=self.hallway_movie)
        self.nextrely += 2 
        self.back_button = self.add(npyscreen.ButtonPress, name='<-- Back', when_pressed_function=self.back)
    def back(self):
        self.parentApp.switchFormPrevious()
    def office_movie(self):
        play_videoFile("Office.mp4", mirror=False)
    def kitchen_movie(self):
        play_videoFile("Kitchen.mp4", mirror=False)
    def hallway_movie(self):
        play_videoFile("Hallway.mp4", mirror=False)

class hvacMenuForm(npyscreen.FormBaseNew):
    def create(self):
        hvactext = "This menu controls the ventilation throughout Iridium Corp Headquarters."
        self.hvac_details = self.add(npyscreen.TitleText,name="Description", value=hvactext, editable=False)
        self.nextrely += 2 
        self.back_button = self.add(npyscreen.ButtonPress, name='<-- Back', when_pressed_function=self.back)
    def back(self):
        self.parentApp.switchFormPrevious()

class MyApplication(npyscreen.NPSAppManaged):
   def onStart(self):
       self.addForm('MAIN', mainMenuForm, name='Main Menu', )
       self.addForm('DOORFORM', doorMenuForm, name='Door Operations')
       self.addForm('PASSWORDFORM', passwordInputForm, name="Password")
       self.addForm('HVACFORM', hvacMenuForm, name="Heating Ventilation & Air Conditioning")
       self.addForm('SURVEILLANCEFORM', surveillanceMenuForm, name="Surveillance System Operations")
       self.addForm('SURVEILLANCEDETAILSFORM', surveillanceDetailsForm, name="Surveillance System Details")
       self.addForm('DOORDETAILSFORM', doorDetailsForm, name="Door Information")
       self.addForm('FOOTAGEFORM', footageForm, name="Available Camera Feeds")
        # A real application might define more forms here.......

if __name__ == '__main__':
   TestApp = MyApplication().run()

