from pynput.keyboard import Key, Controller as KeyboardController
#from pynput.mouse import Button, Controller as MouseController
from time import sleep
#import microbit as mb
#import microbit.radio as radio
import serial

import datetime as dt

keyboard = KeyboardController()
#mouse = MouseController()

### Computer Code ###

ser = serial.Serial(port='/dev/ttyACM0', baudrate=115200, parity='N',stopbits=1, bytesize=8)

print(ser)

while True:
    if ser.is_open:
        print("open")
        msg = ser.read_until().decode('utf-8')
        print(msg)
        if msg.strip().lower() == 'up':
            print('{}'.format(dt.datetime.now()))
            keyboard.press(Key.up)
            keyboard.release(Key.up)
        elif msg.strip().lower() == 'down':
            print('{}'.format(dt.datetime.now()))
            keyboard.press(Key.down)
            keyboard.release(Key.down)
        elif msg.strip().lower() == 'left':
            print('{}'.format(dt.datetime.now()))
            keyboard.press(Key.left)
            keyboard.release(Key.left)
        elif msg.strip().lower() == 'right':
            print('{}'.format(dt.datetime.now()))
            keyboard.press(Key.right)
            keyboard.release(Key.right)
        elif msg.strip().lower() == 'shake':
            print('{}'.format(dt.datetime.now()))
            keyboard.press(Key.space)
            keyboard.release(Key.space)
        elif msg.strip().lower() == 'a':
            print('{}'.format(dt.datetime.now()))
            keyboard.press('A')
            keyboard.release('A')
        elif msg.strip().lower() == 'b':
            print('{}'.format(dt.datetime.now()))
            keyboard.press('B')
            keyboard.release('B')
        ser.reset_input_buffer()
    else:
        print('Port Not Open')
        sleep(1)
