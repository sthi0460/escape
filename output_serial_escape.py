import microbit as m
m.uart.init(baudrate=115200, parity=None, stop=1, bits=8)

#screen_fixed = False
#while not screen_fixed:
#    m.pin0.is_touched

while True:
    m.display.show(m.Image.DIAMOND)
    if m.accelerometer.was_gesture('left'):
        m.uart.write('left\n')
        faceup = False
        while faceup == False:
            m.display.show(m.Image.ARROW_E)
            if m.accelerometer.is_gesture('face up'):
                faceup = True

#        m.sleep(2000)
    elif m.accelerometer.was_gesture('right'):
        m.uart.write('right\n')
        faceup = False
        while faceup == False:
            m.display.show(m.Image.ARROW_W)
            if m.accelerometer.is_gesture('face up'):
                faceup = True
    elif m.accelerometer.was_gesture('up'):
        m.uart.write('down\n')
#        m.sleep(2000)    
        faceup = False
        while faceup == False:
            m.display.show(m.Image.ARROW_N)
            if m.accelerometer.is_gesture('face up'):
                faceup = True
    elif m.accelerometer.was_gesture('down'):
        m.uart.write('up\n')
#        m.sleep(2000)
        faceup = False
        while faceup == False:
            m.display.show(m.Image.ARROW_S)
            if m.accelerometer.is_gesture('face up'):
                faceup = True
    elif m.accelerometer.was_gesture('shake'):
        m.uart.write('shake\n')
        diamond_list = [m.Image.DIAMOND, m.Image.DIAMOND_SMALL, m.Image.DIAMOND, m.Image.DIAMOND_SMALL]
        m.display.show(diamond_list*2, loop=False, delay=100 )

    else:
        pass
    if m.button_a.was_pressed():
        m.uart.write(str('A\n'))
    elif m.button_b.was_pressed():
        m.uart.write(str('B\n'))
    else:
        pass

